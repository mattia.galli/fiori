//prima classe Fiore (corrisponde a una riga) 4 attributi double e uno String
public class Fiore{
    private double sepal_length, sepal_width, petal_length, petal_width;
    String nome;

    public Fiore(){

    }

    public Fiore(double sepal_length, double sepal_width, double petal_length, double petal_width, String nome){
        this.sepal_length = sepal_length;
        this.sepal_width = sepal_width;
        this.petal_length = petal_length;
        this.petal_width = petal_width;
        this.nome = nome;
    }

    public Fiore(Fiore f){
        this.sepal_length = f.getSepal_length();
        this.sepal_width = f.getSepal_width();
        this.petal_length = f.getPetal_length();
        this.petal_width = f.getPetal_width();
        this.nome = f.getNome();
    }

    public double getSepal_length() {
        return this.sepal_length;
    }

    public void setSepal_length(double sepal_length) {
        this.sepal_length = sepal_length;
    }

    public double getSepal_width() {
        return this.sepal_width;
    }

    public void setSepal_width(double sepal_width) {
        this.sepal_width = sepal_width;
    }

    public double getPetal_length() {
        return this.petal_length;
    }

    public void setPetal_length(double petal_length) {
        this.petal_length = petal_length;
    }

    public double getPetal_width() {
        return this.petal_width;
    }

    public void setPetal_width(double petal_width) {
        this.petal_width = petal_width;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean equals(Object o){
        if(this == o){
            return true;
        } else if(o instanceof Fiore){
            Fiore f = (Fiore) o;
            return this.getPetal_length() == f.getPetal_length() 
            && this.getPetal_width() == f.getPetal_width()
            && this.getSepal_length() == f.getSepal_length()
            && this.getSepal_width() == f.getSepal_width()
            && this.getNome().equals(f.getNome());
        }
        else{
            return false;
        }
    }

    public Fiore clone(Fiore f){
        return new Fiore(f.getPetal_length(), f.getPetal_width(), f.getSepal_length(), f.getSepal_width(), f.getNome());
    }
    public String toString(){
        String s = 
        "Nome : " + this.getNome() + System.lineSeparator() +
        "Petal Lenght : " + this.getPetal_length() + System.lineSeparator() +
        "Petal Width : " + this.getPetal_width() + System.lineSeparator() +
        "Sepal Lenght : " + this.getSepal_length() + System.lineSeparator() +
        "Sepal Width : " + this.getSepal_width() + System.lineSeparator();
        return s;
    }
 
    public double distanza(Fiore f){
        return
            Math.sqrt(
                Math.pow((f.petal_length - this.petal_length), 2) +
                Math.pow((f.petal_width - this.petal_width), 2) +
                Math.pow((f.sepal_length - this.sepal_length), 2) +
                Math.pow((f.sepal_width - this.sepal_width), 2)
            );
    }

    public double distanza(double petal_length, double petal_width, double sepal_length, double sepal_width){
        return
            Math.sqrt(
                Math.pow((petal_length - this.petal_length), 2) +
                Math.pow((petal_width - this.petal_width), 2) +
                Math.pow((sepal_length - this.sepal_length), 2) +
                Math.pow((sepal_width - this.sepal_width), 2)
            );
    }


}