import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class DataSetIris {
    ArrayList<Fiore> lista = new ArrayList<>();

    public void loadData(String nomeFile) throws IOException{
            Scanner scanner = new Scanner(new File(nomeFile)).useDelimiter(System.lineSeparator());
            try {
            while(scanner.hasNext()){
                String[] valori = scanner.nextLine().split(",");
                lista.add(new Fiore (Double.valueOf(valori[0]), Double.valueOf(valori[1]), Double.valueOf(valori[2]), Double.valueOf(valori[3]), valori[4]));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            scanner.close();
        }
    }

    public void riordina(Fiore f){
        Collections.sort(lista, (fiore1, fiore2) -> {double d1 = f.distanza(fiore1); double d2 = f.distanza(fiore2); return Double.compare(d1, d2); });
    }

    public String KNN(double petal_length, double petal_width, double sepal_length, double sepal_width, int k){
        ArrayList<Conta> lista = new ArrayList<>();
        riordina(new Fiore(petal_length, petal_width, sepal_length, sepal_width, ""));
        for(Fiore f : this.lista.subList(0, k)){
            Conta c = new Conta(f);
            if(lista.contains(c)){
                lista.get(lista.indexOf(c)).incrementa();
            }
            else{
                lista.add(c);
            }
        }
        Collections.sort(lista);
        
        return lista.get(0).getNome();
    }

    public String KNN1(double petal_length, double petal_width, double sepal_length, double sepal_width){
        return KNN(petal_length, petal_width, sepal_length, sepal_width, 1);
    }

    public String toString(){
        String s = "";
        for(Fiore f : lista){
            s += f.toString() + System.lineSeparator();
        }
        return s;
    }
}
