public class Conta implements Comparable<Conta>{
    private String nome;
    private int conta = 0;

    public Conta(String nome){
        this.nome = nome;
        conta++;
    }

    public Conta(Fiore f){
        this.nome = f.getNome();
        conta++;
    }

    public void incrementa(){
        conta++;
    }

    public String getNome() {
        return this.nome;
    }

    public int getConta() {
        return this.conta;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o){
            return true;
        }
        else if(o instanceof Conta){
            Conta c = (Conta) o;
            return c.getNome().equals(nome);
        }
        else{
            return false;
        }
    }
    
    public int compareTo(Conta c){
        return - (conta - c.conta);
    }

}
